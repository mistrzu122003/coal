# COAL

Collaboratively Organised Abuse List

This is a global abuse list that apps can make use of to ban/restrict or highlight users that have been identified
as abusers of the Blurt reward pool, with infringements such as farming rewards, copy-pasting repetitive content, fake accounts, plagiarism, et cetera.

Please refer to coal.json for the curated abuse list. Please submit merge requests for additions or appeals.

## Key

Below is a list of reason codes that can be used when adding an account to COAL.

+ PLG - Plagiarism
+ FARM - Farming Rewards: making multiple low-effort (possibly automated) posts and self-voting (or voting with alt accounts) for the purpose of extracting rewards without adding value.
+ CFARM - Comment Farming: posting multiple low-effort (possibly automated) comments and self-voting (or voting with alt accounts) for the purpose of extracting rewards without adding value.
+ CPC - Copy-Pasted-Content: Copy/pasted content without citing source or adding significant own content.
+ SPAM - Making a large number of posts and/or comments that are of very low value to the platform, or making the same comment over and over to multiple users, effectively bloating the chain with useless data.
+ FAKE - Fake account/indentity theft/phished accounts.
+ OTHER - Uncategorised, leave explanation in the note.

If there is more than one reason, use a comma-separated list as in the following example.

```json
{
    "bad-account": {
        "reason": "PLG, FARM, SPAM",
        "notes": "User posts plagiarized content several times per day and upvotes his own posts with a bunch of alt accounts."
    }
}
```

Please make sure to be clear and concise with your notes.



Naughty children get lumps of COAL in their stockings for Xmas! :)
